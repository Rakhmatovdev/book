import axios from "axios";
import appConfig from "../configs/appConfig";
import store from "../store";
import { setSignedInSucces } from "../store/auth/sessionSlice";

const unAuntificatedCode=['403']

const api=axios.create({
    timeout:10000,
    baseURL:appConfig.apiPrefix
})

api.interceptors.request.use(
(config)=>{
const {session}=store.getState()
if(session.token !== ""){
config.headers['Authorization']=`${session.token} ${appConfig.tokenType}`
}
    return config
},
(error)=>Promise.reject(error))

api.interceptors.request.use(
    
(response)=>response,
(error)=>{
    const {response}=error
    if(unAuntificatedCode.includes(response.status)){
     return store.dispatch(setSignedInSucces())
    }
   return  Promise.reject(error)
})

export default api