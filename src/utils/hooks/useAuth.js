import { useDispatch } from "react-redux";
import api from "services";
import API_ENDPOINTS from "services/ApiEndpoints";
import { setSignedInSucces } from "store/auth/sessionSlice";
import { setUser } from "store/auth/userSlice";

const useAuth = () => {
  const dispatch = useDispatch();

  const SignUp = async (values) => {
    try {
      const res = await api.post(API_ENDPOINTS.REGISTER, values);
      dispatch(setSignedInSucces(res.data.tokens.access));
      dispatch(setUser(res.data.tokens.user));
      return {
        success: true,
        message: ''
    }
    } catch (error) {
        return {success: false,
             message:JSON.stringify(error.response.data)}

    }
  };
  const SignIn = async (values) => {
    try {
      const res = await api.post(API_ENDPOINTS.LOGIN, values);
      dispatch(setSignedInSucces(res.data.tokens.access));
      dispatch(setUser(res.data?.user || {
        first_name: 'user',
        last_name: 'last name',
        phone_number: null,
        email: null
    }))
    return {
        success: true,
        message: ''
    }
} catch (error) {
    return {
        success: false,
        message: JSON.stringify(error.response.data)
    }
}
  };
  const SignOut = async () => {
    console.log("SignOut");
  };

  return { SignIn, SignOut, SignUp };
};

export default useAuth;
