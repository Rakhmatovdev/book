import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import AuthRoute from "../components/route/AuthRoute";
import ProtectedRoute from "../components/route/ProtectedRoute";
import { AuthRoutes, ProtectedRoutes } from "../configs/routes";
import AppRoute from "../components/route/AppRoute";
import ChechAuth from "../components/route/ChechAuth";
import { useSelector } from "react-redux";

const AllPages = () => {

  const {signedIn}=useSelector(state=>state.session)
  const {role}=useSelector(state=>state.user)
  return (
    <Suspense fallback={<p>Loading...</p>}>
      <Routes>
        <Route path="/" element={<ProtectedRoute isAuthentificated={signedIn} />}>
          {ProtectedRoutes.map((route) => (
            <Route
              key={route.key}
              path={route.path}
              element={
                <ChechAuth role={route.role} userRole={role}>
              <AppRoute component={route.component} />
              </ChechAuth>
            }
            />
          ))}
        </Route>
        <Route path="/" element={<AuthRoute isAuthentificated={signedIn} />}>
          {AuthRoutes.map((route) => (
            <Route
              key={route.key}
              path={route.path}
              element={<AppRoute component={route.component} />}
            />
          ))}
        </Route>
        <Route path="/" element={<ProtectedRoute />} />
        <Route path="*" element={<p>Not Found</p>} />
      </Routes>
    </Suspense>
  );
};

export default AllPages;
