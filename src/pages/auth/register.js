import React, { useState } from "react";
import PhoneInput from "react-phone-input-2";
import { useForm } from "react-hook-form";
import "react-phone-input-2/lib/style.css";
import { Link } from "react-router-dom";
import useAuth from "utils/hooks/useAuth";
import toast, { Toaster } from "react-hot-toast";
// import HttpClient from '../../services/universal date/HttpClient';
// import API_ENDPOINTS from '../../services/universal date/ApiEndpoints';

const Register = () => {
  const { SignUp } = useAuth();
  const {
    register,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm();
  const [phone, setPhone] = useState(null);
  const onSubmit = async (data) => {
    if (!phone) {
      alert("Iltimos nomeringizni kiriting !");
    } else {
      const newData = { ...data, phone_number: phone };
      console.log(watch("name" + 11)); //
      const resp = await SignUp(newData);
      !resp.success && toast.error(resp.message);
      reset();
      setPhone(null);
    }
  };

  return (
    <div className="  ">
      <div className=" flex mt-28  px-20 justify-center md:justify-between">
        <div className="img">
          <img src="/images/register-cover-img.png" alt="" />
        </div>
        <form
          className=" flex flex-col items-center  flex-1"
          onSubmit={handleSubmit(onSubmit)}
        >
          <h2 className="text-4xl mt-1 font-semibold tracking-wider">
            Ro'yxatdan otish
          </h2>
          <p className="mt-8">
            Sizning hisobingiz bormi ?
            <Link
              to="/login"
              className="text-sky-500 cursor-pointer hover:text-fuchsia-500 hover:underline"
            >
              {" "} Kirish
            </Link>
          </p>
          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-xl ">
            <input
              type="text"
              className="text-lg outline-none w-full"
              placeholder="Ism..."
              {...register("firs_name")}
              required
            />
          </label>
          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-xl ">
            <input
              type="text"
              className="text-lg outline-none w-full"
              placeholder="Familya..."
              {...register("last_name", { required: true })}
              required
            />
          </label>
          {errors.lname && <span>This field is required</span>}

          <label className="mt-8   w-[400px] ">
            <PhoneInput
              value={phone}
              onChange={(e) => setPhone(e)}
              inputStyle={{
                fontSize: "18px",
                width: "400px",
                outline: "none",
                height: "100%",

                padding: "15px 40px",
                alignItems: "center",
              }}
              inputProps={{
                required: true,
              }}
              specialLabel=""
              country={"uz"}
              placeholder="Enter your number..."
            />
          </label>
          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-xl ">
            <input
              type="email"
              className="text-lg outline-none w-full"
              placeholder="Email..."
              {...register("email")}
              required
            />
          </label>
          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-xl ">
            <input
              type="password"
              className="text-lg outline-none w-full"
              placeholder="Password..."
              {...register("password")}
              required
            />
          </label>
          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-full cursor-pointer text-center bg-sky-900 text-white text-xl ">
            <input
              type="submit"
              value={"Ro'yxatdan otish"}
              className=" outline-none"
              placeholder="Password..."
            />
          </label>
        </form>
      </div>
      <Toaster position="top-center" reve />
    </div>
  );
};

export default Register;
