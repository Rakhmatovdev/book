import React from "react";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import useAuth from "utils/hooks/useAuth";
import toast, { Toaster } from "react-hot-toast";

const Login = () => {
  const {
    register,
    handleSubmit,
    watch,
    reset,
    formState: { errors },
  } = useForm();

  const { SignIn } = useAuth();

  const onSubmit = async (data) => {
    const resp = await SignIn(data);
    !resp.success && toast.error(resp.message);
    toast("Hello Jasur!", {
      icon: "👏",
      duration: 2000,
      style: {
        borderRadius: "10px",
        background: "#333",
        color: "#fff",
      },
    });
    console.log(watch("name")); //
    reset();
  };

  return (
    <div className="container  mx-auto ">
      <div className=" flex mt-28 gap-12 flex-wrap justify-center md:justify-between">
        <div className="img">
          <img src="/images/login-cover-img.png" alt="" />
        </div>
        <form
          className=" flex flex-col items-center  flex-1"
          onSubmit={handleSubmit(onSubmit)}
        >
          <h2 className="text-4xl mt-4 md:mt-28 font-semibold tracking-wider">
            Kirish
          </h2>
          <p className="mt-8">
            Sizning hisobingiz yo'qmi ?
            <Link
              to="/register"
              className="text-sky-500 cursor-pointer hover:text-fuchsia-500 hover:underline"
            >
              {" "}
              Ro'yxatdan o'tish
            </Link>
          </p>
          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-xl ">
            <input
              type="email"
              className="text-lg outline-none"
              placeholder="Email..."
              {...register("email")}
              required
            />
          </label>
          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-xl ">
            <input
              type="password"
              className="text-lg outline-none"
              placeholder="Password..."
              {...register("pwd")}
              required
            />
          </label>
          {errors.email && <span>This field is email</span>}
          {errors.pwd && <span>This field is password</span>}

          <label className=" border mt-8 px-6 py-3 w-[400px] rounded-full cursor-pointer text-center bg-sky-900 text-white text-xl ">
            <input
              type="submit"
              value={"Kirish"}
              className=" outline-none"
              placeholder="Password..."
            />
          </label>
        </form>
      </div>
      <Toaster position="top-right" reverseOrder={false} />
    </div>
  );
};

export default Login;
