
import React from 'react'
import { Navigate, Outlet } from 'react-router-dom';

const AuthRoute = ({isAuthentificated}) => {

  if(isAuthentificated){
      return <Navigate to={'/'} replace/>
  }
  return <Outlet/>
}

export default AuthRoute