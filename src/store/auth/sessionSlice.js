import { createSlice } from "@reduxjs/toolkit";

const accessToken=localStorage.getItem('accessToken')

const initialState={
token:accessToken || "",
signedIn:accessToken?true:false
}

export const sessionSlice=createSlice({
    name:"auth/session",
    initialState,
    reducers:{
        setSignedInSucces:(state,action)=>{
            state.signedIn=true;
                 state.token=action.payload;
                 localStorage.setItem('accessToken',action.payload)

        },
        setSignedOutSucces:(state)=>{
             state.signedIn=false
            state.token=''
        },
    }

})


export const {setSignedInSucces,setSignedOutSucces}=sessionSlice.actions

export default sessionSlice.reducer