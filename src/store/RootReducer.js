import { combineReducers } from "redux"
import session from "./auth/sessionSlice"
import user from "./auth/userSlice"

const rootReducer=combineReducers({
    user,
    session,
})

export default rootReducer